import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        fontFamily: 'Montserrat',
      ),
      home: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          elevation: 0,
          backgroundColor: Colors.white,
          actions: <Widget>[
            Padding(
              padding: EdgeInsets.only(right: 20, top: 20),
              child: Text(
                'Skip',
                style: TextStyle(
                    color: Colors.grey,
                    fontSize: 18,
                    fontWeight: FontWeight.w400),
              ),
            )
          ],
        ),
        body: AppIntro(),
      ),
    );
  }
}

class AppIntro extends StatefulWidget {
  @override
  _AppIntroState createState() => _AppIntroState();
}

class _AppIntroState extends State<AppIntro> {
  final PageController controller = PageController(initialPage: 0);
  int currentIndex = 0;
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Stack(
        alignment: Alignment.bottomCenter,
        children: [
          PageView(
            onPageChanged: (int page) {
              setState(() {
                currentIndex = page;
              });
            },
            controller: controller,
            children: [
              makePage(
                title: 'Craft, write and play',
                subtitle:
                    'Driven by teamwork, digital solutions and the people that use them.',
                image: 'assets/images/page1.jpg',
              ),
              makePage(
                title: 'We bring ideas to life',
                subtitle:
                    'We\'re a team of designers and brand strategists who create inspiring apps.',
                image: 'assets/images/page2.jpg',
              ),
              makePage(
                title: 'Marketing videos that perform and attaract users',
                subtitle:
                    'Experts in visual production maximize return of impressions with measurably better video creative.',
                image: 'assets/images/page3.jpg',
              ),
            ],
          ),
          Container(
            // color: Colors.red,
            child: Padding(
              padding: EdgeInsets.only(bottom: 15),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: makeIndicators(),
              ),
            ),
          )
        ],
      ),
    );
  }

  List<Widget> makeIndicators() {
    List<Color> colors = [
      Color(0XFFF37BA9),
      Color(0XFF69CAD3),
      Color(0XFF600660),
    ];
    List<Widget> indicators = [];
    for (int i = 0; i < 3; i++) {
      indicators.add(makeIndicator(currentIndex == i, colors[currentIndex]));
    }
    return indicators;
  }

  Widget makeIndicator(bool isActive, Color color) {
    const double indicatorSize = 8;
    return AnimatedContainer(
      duration: Duration(milliseconds: 300),
      height: indicatorSize,
      width: isActive ? 35 : indicatorSize,
      margin: EdgeInsets.only(
        left: 5,
      ),
      decoration: BoxDecoration(
        color: color,
        borderRadius: BorderRadius.all(
          Radius.circular(indicatorSize / 2),
        ),
      ),
    );
  }
}

Widget makePage({@required image, @required title, @required subtitle}) {
  return Container(
    // color: Colors.white,
    child: Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
          child: ClipRRect(
            borderRadius: BorderRadius.circular(30),
            child: Image(
              image: AssetImage(
                image,
              ),
            ),
          ),
        ),
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
          child: Text(
            title,
            textAlign: TextAlign.center,
            style: TextStyle(
              fontSize: 30,
              fontWeight: FontWeight.bold,
              fontFamily: 'Pacifico',
            ),
          ),
        ),
        SizedBox(
          height: 20,
        ),
        Padding(
          padding: EdgeInsets.all(15),
          child: Text(
            subtitle,
            textAlign: TextAlign.center,
            style: TextStyle(
              color: Colors.grey.shade700,
              fontSize: 17,
              fontWeight: FontWeight.bold,
              fontFamily: 'Montserrat',
            ),
          ),
        ),
      ],
    ),
  );
}
